﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverHandler : MonoBehaviour {
    bool isHovered;

    Renderer rend;
    Vector3 initialScale;
    Vector3 finalScale;
    Vector3 currentScale;

    [Range(1, 10)]
    public int timeScale = 1;

    [Range(1, 10)]
    public float scaleMultiplier = 2;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        initialScale = transform.localScale;
        finalScale = initialScale * scaleMultiplier;
    }

    void OnMouseEnter()
    {
        rend.material.color = Color.red;
        StartCoroutine("ScaleUp", 1f);
    }

    void OnMouseExit()
    {
        rend.material.color = Color.white;
        StartCoroutine("ScaleDown", 1f);
    }

    IEnumerator ScaleUp(float tDuration)
    {
        float duration = tDuration;
        float startTime = Time.time;
        float endTime = startTime + duration;

        StopCoroutine("ScaleDown");
        currentScale = transform.localScale;
       
        while (Time.time <= endTime)
        {
            float t = (Time.time - startTime) / duration;
            transform.localScale = Vector3.Lerp(currentScale, finalScale, t);
            yield return null;
        }
    }

    IEnumerator ScaleDown(float tDuration)
    {
        float duration = tDuration;
        float startTime = Time.time;
        float endTime = startTime + duration;

        StopCoroutine("ScaleUp");
        currentScale = transform.localScale;

        while (Time.time <= endTime)
        {
            float t = (Time.time - startTime) / duration;
            transform.localScale = Vector3.Lerp(currentScale, initialScale, t);
            yield return null;
        }
    }
}
